#+OPTIONS:  org-hugo-export-with-toc:2
#+OPTIONS: toc:nil
#+AUTHOR: XLH
#+TITLE: Crypto Tuto v2
#+DATE: 2022-12-28
#+DESCRIPTION: Tuto sur l'emploi des clés de cryptage GPG
#+HUGO_BASE_DIR: ./
#+HUGO_AUTO_SET_LAST_MOD: t
#+HUGO_WEIGHT: auto
#+HUGO_TAGS: linux
#+HUGO_CATEGORIES: Crypto
#+HUGO_SERIES: Crypto
#+TOC: headlines 2


#+PROPERTY: header-args:emacs-lisp :tangle init.el :noweb yes

Nota: ce tuto est généré à partir des sites
- [https://android24tech.com/comment-chiffrer-et-dechiffrer-des-fichiers-avec-gpg-sous-linux/]
- [[https://jherrlin.github.io/posts/emacs-gnupg-and-pass/][Emacs, GnuPG and Pass]]

* Configuration
** Operating Sytem
#+BEGIN_SRC shell :results output code
  uname -a
#+END_SRC

#+RESULTS:
#+begin_src shell
Linux popeye 6.0.0-0.deb11.2-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.0.3-1~bpo11+1 (2022-10-29) x86_64 GNU/Linux
#+end_src
** GnuPG
#+BEGIN_SRC shell :results output code
  gpg --version
#+END_SRC

#+RESULTS:
#+begin_src shell
gpg (GnuPG) 2.2.27
libgcrypt 1.8.8
Copyright (C) 2021 Free Software Foundation, Inc.
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: /home/prog/.gnupg
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
#+end_src
** Pass
#+BEGIN_SRC shell :results output code
  pass --version
#+END_SRC

#+RESULTS:
#+begin_src shell
============================================
= pass: the standard unix password manager =
=                                          =
=                  v1.7.3                  =
=                                          =
=             Jason A. Donenfeld           =
=               Jason@zx2c4.com            =
=                                          =
=      http://www.passwordstore.org/       =
============================================
#+end_src

** Emacs
#+BEGIN_SRC shell :results output code
  emacs --version
#+END_SRC

#+RESULTS:
#+begin_src shell
GNU Emacs 29.0.50
Copyright (C) 2022 Free Software Foundation, Inc.
GNU Emacs comes with ABSOLUTELY NO WARRANTY.
You may redistribute copies of GNU Emacs
under the terms of the GNU General Public License.
For more information about these matters, see the file named COPYING.
#+end_src

** Config file
#+BEGIN_SRC shell :results output code
  cat ~/.gnupg/gpg.conf
#+END_SRC

#+RESULTS:
#+begin_src shell
#+end_src


* Générer une clé
Il suffit de lancer la commande:
=gpg --full-generate-key= et de répondre aux différentes questions:
** Type de clé:
Par défaut prendre RSA
#+BEGIN_SRC txt
❯ gpg --full-generate-key
gpg (GnuPG) 2.2.19; Copyright (C) 2019 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Sélectionnez le type de clef désiré :
   (1) RSA et RSA (par défaut)
   (2) DSA et Elgamal
   (3) DSA (signature seule)
   (4) RSA (signature seule)
  (14) Existing key from card
Quel est votre choix ? 1
les clefs RSA peuvent faire une taille comprise entre 1024 et 4096 bits.
Quelle taille de clef désirez-vous ? (3072)
#+END_SRC
Vous devez choisir une longueur de bit pour les clés de chiffrement. Appuyez sur Entrée pour accepter la valeur par défaut.

** Durée validité de la clé
Vous devez spécifier la durée de la clé. Si vous testez le système, entrez une courte durée comme 5 pour cinq jours. Si vous souhaitez conserver cette clé, saisissez une durée plus longue, comme 1 an pendant un an. La clé durera 12 mois et devra donc être renouvelée après un an. Confirmez votre choix avec un Y.

Vous devez saisir votre nom et votre adresse e-mail. Vous pouvez ajouter un commentaire si vous le souhaitez.

#+begin_src txt
La taille demandée est 3072 bits
Veuillez indiquer le temps pendant lequel cette clef devrait être valable.
         0 = la clef n'expire pas
      <n>  = la clef expire dans n jours
      <n>w = la clef expire dans n semaines
      <n>m = la clef expire dans n mois
      <n>y = la clef expire dans n ans
Pendant combien de temps la clef est-elle valable ? (0) 5
La clef expire le jeu. 06 janv. 2022 08:32:32 CET
Est-ce correct ? (o/N)
#+end_src
** Attribution et création de la clé

Après avoir complété les informations sur le nom et le mail associé (et éventuellement un commentaire), la clé est générée (cela peut prendre quelques minutes suivant la taille et la puissance de l'ordinateur)
#+begin_src txt
Nom réel : Xavier TEST
Adresse électronique : xaviertest@toto.com
Commentaire :
Vous avez sélectionné cette identité :
    « Xavier TEST <xaviertest@toto.com> »

Changer le (N)om, le (C)ommentaire, l'(A)dresse électronique
ou (O)ui/(Q)uitter ? O
#+end_src
Vous serez invité à saisir votre phrase secrète. Vous aurez besoin de la phrase secrète chaque fois que vous travaillez avec vos clés, alors assurez-vous de savoir de quoi il s’agit.

Clique le =OK= lorsque vous avez saisi votre mot de passe. Cette fenêtre s’affiche lorsque vous travaillez avec =gpg=, alors assurez-vous de vous souvenir de votre phrase secrète.

* Génération d’un certificat de révocation
Si votre clé privée devient connue des autres, vous devrez dissocier les anciennes clés de votre identité afin de pouvoir en générer de nouvelles. Pour ce faire, vous aurez besoin d’un certificat de révocation. Nous allons le faire maintenant et le stocker dans un endroit sûr.

le =--output= L’option doit être suivie du nom de fichier du certificat que vous souhaitez créer. le =--gen-revoke= l’option provoque gpg pour générer un certificat de révocation. Vous devez fournir l’adresse e-mail que vous avez utilisée lors de la génération des clés.
#+begin_src txt
   gpg --output ~/revocation.crt --gen-revoke dave-geek@protonmail.com
#+end_src

Il vous sera demandé de confirmer que vous souhaitez générer un certificat. pressez Y et appuyez sur Entrée. Il vous sera demandé la raison pour laquelle vous générez le certificat. Comme nous le faisons à l’avance, nous n’en sommes pas sûrs. presse 1 comme une supposition plausible et appuyez sur Entrée.

Vous pouvez saisir une description si vous le souhaitez. Appuyez deux fois sur Entrée pour terminer votre description.

Il vous sera demandé de confirmer vos paramètres, appuyez sur Y et appuyez sur Entrée.

Le certificat sera généré. Vous verrez un message renforçant la nécessité de protéger ce certificat.

Il mentionne quelqu’un appelé Mallory. Les discussions sur la cryptographie ont longtemps utilisé Bob et Alice comme deux personnes communiquant. Il existe d’autres personnages secondaires. Eve est une écoute indiscrète, Mallory est un attaquant malveillant. Tout ce que nous devons savoir, c’est que nous devons garder le certificat en sécurité.

Supprimons au minimum toutes les autorisations, à l’exception des nôtres, du certificat.

#+begin_src txt
   chmod 600 ~/revocation.crt
#+end_src

Vérifions avec =ls= pour voir quelles sont les autorisations maintenant:
#+begin_src txt
   ls -l
#+end_src

C’est parfait. Personne à part le propriétaire du fichier – nous – ne peut rien faire avec le certificat.

* Importation de la clé publique de quelqu’un d’autre
Pour crypter un message qu’une autre personne peut décrypter, nous devons avoir sa clé publique.

Si vous avez reçu leur clé dans un fichier, vous pouvez l’importer avec la commande suivante. Dans cet exemple, le fichier de clé est appelé « mary-geek.key ».
#+begin_src txt
   gpg --import mary-geek.key
#+end_src
La clé est importée et le nom et l’adresse e-mail associés à cette clé s’affichent. De toute évidence, cela devrait correspondre à la personne de qui vous l’avez reçu.

Il est également possible que la personne dont vous avez besoin d’une clé ait téléchargé sa clé sur un serveur de clés publiques. Ces serveurs stockent les clés publiques des utilisateurs du monde entier. Les serveurs de clés se synchronisent périodiquement afin que les clés soient universellement disponibles.

Le serveur de clés publiques du MIT est un serveur de clés populaire et régulièrement synchronisé, la recherche doit donc réussir. Si quelqu’un n’a téléchargé que récemment une clé, l’affichage peut prendre quelques jours.

le =--keyserver= L’option doit être suivie du nom du serveur clé que vous souhaitez rechercher. le =--search-keys= L’option doit être suivie soit du nom de la personne recherchée, soit de son adresse e-mail. Nous utiliserons l’adresse e-mail:

#+begin_src txt
   gpg --keyserver pgp.mit.edu --search-keys mary-geek@protonmail.com
#+end_src

Les matchs sont répertoriés pour vous et numérotés. Pour en importer un, saisissez le numéro et appuyez sur Entrée. Dans ce cas, il y a une seule correspondance, nous tapons donc =1= et appuyez sur Entrée.

La clé est importée et le nom et l’adresse e-mail associés à cette clé s’affichent.

* Vérification et signature d’une clé
Si un fichier de clé publique vous a été remis par une personne que vous connaissez, vous pouvez sans risque dire qu’il appartient à cette personne. Si vous l’avez téléchargée à partir d’un serveur de clés publiques, vous pouvez ressentir le besoin de vérifier que la clé appartient à la personne à laquelle elle est destinée.

L'option =--fingerprint= provoque gpg pour créer une courte séquence de dix ensembles de quatre caractères hexadécimaux. Vous pouvez demander à la personne de vous envoyer l’empreinte de sa clé.

Vous pouvez ensuite utiliser l'option =--fingerprint= pour générer la même séquence d’empreintes digitales de caractères hexadécimaux et les comparer. S’ils correspondent, vous savez que la clé appartient à cette personne.
#+begin_src txt
   gpg --fingerprint mary-geek@protonmail.com
#+end_src

L’empreinte digitale est générée.

Lorsque vous êtes convaincu que la clé est authentique et appartient à la personne à laquelle elle est censée être associée, vous pouvez signer sa clé.

Si vous ne le faites pas, vous pouvez toujours l’utiliser pour crypter et décrypter les messages de et vers cette personne. Mais =gpg= vous demandera à chaque fois si vous souhaitez continuer car la clé n’est pas signée. Nous allons utiliser l'option =--sign-key=n et fournissez l’adresse e-mail de la personne, afin que gpg sait quelle clé signer.

#+begin_src txt
   gpg --sign-key mary-geek@protonmail.com
#+end_src

Vous verrez des informations sur la clé et la personne, et il vous sera demandé de vérifier que vous souhaitez vraiment signer la clé. presse =Y= et appuyez sur Entrée pour signer la clé.

* Comment partager votre clé publique
Pour partager votre clé sous forme de fichier, nous devons l’exporter depuis =gpg= magasin de clés local. Pour ce faire, nous utiliserons l'option =--export=, qui doit être suivie de l’adresse e-mail que vous avez utilisée pour générer la clé. L'option =--output= doit être suivie du nom du fichier dans lequel vous souhaitez exporter la clé. L'option =--armor= indique =gpg= pour générer une sortie d’armure ASCII au lieu d’un fichier binaire.

#+begin_src txt
   gpg --output ~/dave-geek.key --armor --export dave-geek@protonmail.com
#+end_src

Nous pouvons jeter un œil à l’intérieur du fichier clé avec =less=.

#+begin_src txt
   less dave-geek.key
#+end_src

Vous pouvez également partager votre clé publique sur un serveur de clés publiques. L'option =--send-keys= envoie la clé au serveur de clés. L'option =--keyserver= doit être suivie de l’adresse Web du serveur de clé publique. Pour identifier la clé à envoyer, l’empreinte de la clé doit être fournie sur la ligne de commande. Notez qu’il n’y a pas d’espaces entre les ensembles de quatre caractères.

(Vous pouvez voir l’empreinte digitale de votre clé en utilisant l'option =--fingerprint=.)
#+begin_src txt
   gpg --send-keys --keyserver pgp.mit.edu 31A4E3BE6C022830A804DA0EE9E4D6D0F64EEED4
#+end_src

Vous recevrez une confirmation que la clé a été envoyée.

* Chiffrement des fichiers
Nous sommes enfin prêts à crypter un fichier et à l’envoyer à Mary. Le fichier s’appelle Raven.txt.

L'option =--encrypt= indique =gpg= pour crypter le fichier, et l'option =--sign= lui dit de signer le fichier avec vos coordonnées. L'option =--armor= indique à =gpg= de créer un fichier ASCII. L'option =-r= (destinataire) doit être suivie de l’adresse e-mail de la personne à laquelle vous envoyez le fichier.

#+begin_src txt
   gpg --encrypt --sign --armor -r mary-geek@protonmail.com
#+end_src

Le fichier est créé avec le même nom que l’original, mais avec «.asc» ajouté au nom du fichier. Jetons un œil à l’intérieur.

#+begin_src txt
   less Raven.txt.asc
#+end_src
Le fichier est complètement illisible et ne peut être déchiffré que par une personne disposant de votre clé publique et de la clé privée de Mary. La seule personne à avoir les deux devrait être Mary.

Nous pouvons maintenant envoyer le fichier à Mary, confiant que personne d’autre ne peut le décrypter.

* Déchiffrer des fichiers
Mary a envoyé une réponse. Il se trouve dans un fichier crypté appelé coded.asc. Nous pouvons le déchiffrer très facilement en utilisant l'option =--decrypt=. Nous allons rediriger la sortie dans un autre fichier appelé plain.txt.

Notez que nous n’avons pas besoin de le dire gpg de qui provient le fichier. Cela peut fonctionner à partir du contenu crypté du fichier.

#+begin_src txt
   gpg --decrypt coded.asc > plain.txt
#+end_src

Le fichier a été déchiffré avec succès pour nous.


* Rafraîchir vos clés
Périodiquement, vous pouvez demander =gpg= pour vérifier les clés qu’il possède par rapport à un serveur de clés publiques et pour actualiser celles qui ont changé. Vous pouvez le faire tous les quelques mois ou lorsque vous recevez une clé d’un nouveau contact.

L'option =--refresh-keys= provoque gpg pour effectuer la vérification. L'option =--keyserver= doit être suivie par le serveur clé de votre choix. Une fois les clés synchronisées entre les serveurs de clés publiques, peu importe celui que vous choisissez.

#+begin_src txt
   gpg --keyserver pgp.mit.edu --refresh-keys
#+end_src

=gpg= répond en listant les clés qu’il vérifie et en vous faisant savoir si certaines ont changé et ont été mises à jour.
